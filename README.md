# CouchDB Sample Project

This project is an example to access a CouchDB Server in .Net C#, using :

 - NewtonSoft.Json
 - System.Configuration
 - System.Net;
 - System.Net.Http;
 - System.Text.RegularExpressions;
