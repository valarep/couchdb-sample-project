﻿using CouchDbClientExample.Model.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CouchDbClientExample
{
    public partial class Form1 : Form
    {
        Employee employee;
        List<Employee> employees;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnFindById_Click(object sender, EventArgs e)
        {
            employee = Employee.FindById(txtId.Text);
            if (employee != null)
            {
                txtFirstName.Text = employee.FirstName;
                txtLastName.Text = employee.LastName;
                txtAddress.Text = employee.Address;
                dtpBirthdate.Value = employee.BirthDate;
                numSalary.Value = employee.Salary;
            }
            else
            {
                MessageBox.Show("id not found");
            }
        }

        private void btnFindAll_Click(object sender, EventArgs e)
        {
            employees = Employee.FindAll();
            dataGridView.DataSource = bindingSource;
            bindingSource.DataSource = employees;
            //dataGridView.Columns["ObjectId"].Visible = false;
            dataGridView.Columns["RevisionId"].Visible = false;
        }
    }
}
