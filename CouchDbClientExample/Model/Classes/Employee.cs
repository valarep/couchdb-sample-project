﻿using CouchDbClientExample.Model.Dao;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouchDbClientExample.Model.Classes
{
    public class Employee
    {
        #region Properties
        [JsonProperty(PropertyName = "_id")]
        public string ObjectId { get; set; }
        [JsonProperty(PropertyName = "_rev")]
        public string RevisionId { get; set; }
        [JsonProperty(PropertyName = "firstname")]
        public string FirstName { get; set; }
        [JsonProperty(PropertyName = "lastname")]
        public string LastName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        [JsonProperty(PropertyName = "birthdate")]
        public DateTime BirthDate { get; set; }
        public int Age { get { return new DateTime(DateTime.Now.Subtract(BirthDate).Ticks).Year - 1; } }
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }
        [JsonProperty(PropertyName = "salary")]
        public decimal Salary { get; set; }
        #endregion

        #region Methodes
        public static Employee FindById(string id)
        {
            return EmployeeDao.FindById(id);
        }

        public static List<Employee> FindAll()
        {
            return EmployeeDao.FindAll();
        }
        #endregion
    }
}
