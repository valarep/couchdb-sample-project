﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouchDbClientExample.Model.Dal
{
    public class CouchDBListItem<T, U>
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "key")]
        public T Key { get; set; }
        [JsonProperty(PropertyName = "value")]
        public U Value { get; set; }
    }
}
