﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouchDbClientExample.Model.Dal
{
    public class CouchDBList<T, U>
    {
        #region Properties
        /// <summary>
        /// Total rows in response
        /// </summary>
        [JsonProperty(PropertyName = "total_rows")]
        public long TotalRows { get; set; }

        /// <summary>
        /// Current offset number
        /// </summary>
        [JsonProperty(PropertyName = "offset")]
        public long Offset { get; set; }

        /// <summary>
        /// Rows in the offset
        /// </summary>
        [JsonProperty(PropertyName = "rows")]
        public List<CouchDBListItem<T, U>> Rows { get; set; } = new List<CouchDBListItem<T, U>>();

        private List<T> keys;
        public List<T> Keys
        {
            get
            {
                if (keys == null)
                {
                    keys = new List<T>();
                    foreach (CouchDBListItem<T, U> item in Rows)
                    {
                        keys.Add(item.Key);
                    }
                }
                return keys;
            }
        }

        private List<U> values;
        public List<U> Values
        {
            get
            {
                if (values == null)
                {
                    values = new List<U>();
                    foreach (CouchDBListItem<T, U> item in Rows)
                    {
                        values.Add(item.Value);
                    }
                }
                return values;
            }
        }

        #endregion
    }
}
