﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CouchDbClientExample.Model.Dal
{
    public abstract class AbstractDao
    {
        /// <summary>
        /// CouchDB database credentials
        /// </summary>
        public static Credentials Credentials { get; private set; } = new Credentials
        {
            Username = ConfigurationManager.AppSettings.Get("Username"),
            Password = ConfigurationManager.AppSettings.Get("Password")
        };

        /// <summary>
        /// CouchDB server uri
        /// </summary>
        public static Uri Uri { get; private set; } = new Uri(ConfigurationManager.AppSettings.Get("Uri"));

        /// <summary>
        /// HTTP Client to access CouchDB server
        /// </summary>
        public static HttpClient Client { get; private set; } = new HttpClient { BaseAddress = Uri };

        /// <summary>
        /// Authentication Cookie
        /// </summary>
        /// 
        public static Cookie AuthCookie
        {
            get
            {
                Cookie authCookie;

                // converting Credentials to JSON
                string authPayload = JsonConvert.SerializeObject(Credentials);

                // sending Credentials to CouchDB and waiting response
                HttpResponseMessage result = Client.PostAsync("/_session", new StringContent(authPayload, Encoding.UTF8, "application/json")).Result;
                if (result.IsSuccessStatusCode)
                {
                    List<KeyValuePair<string, IEnumerable<string>>> responseHeaders = result.Headers.ToList();
                    string response = result.Content.ReadAsStringAsync().Result;
                    Console.WriteLine("Authenticated user from CouchDB API:");
                    Console.WriteLine(response);
                    string authCookieString = responseHeaders.Where(r => r.Key == "Set-Cookie").Select(r => r.Value.ElementAt(0)).FirstOrDefault();
                    if (authCookieString != null)
                    {
                        string pattern = "^(.*?)=(.*?);";
                        Regex regex = new Regex(pattern);
                        Match match = regex.Match(authCookieString);
                        string name = match.Groups[1].Value;
                        string value = match.Groups[2].Value;
                        authCookie = new Cookie(name, value);
                    }
                    else
                    {
                        throw new Exception("No 'Set-Cookie' key in the headers in the response from the CouchDB API");
                    }
                }
                else
                {
                    throw new HttpRequestException("Authentication failure: " + result.ReasonPhrase);
                }
                return authCookie;
            }
        }

        public AbstractDao()
        {
        }

    }
}
