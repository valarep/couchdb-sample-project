﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouchDbClientExample.Model.Dal
{
    public class CouchDBRevision
    {
        [JsonProperty(PropertyName = "rev")]
        public string Revision { get; set; }
    }
}
