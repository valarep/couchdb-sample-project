﻿using CouchDbClientExample.Model.Classes;
using CouchDbClientExample.Model.Dal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CouchDbClientExample.Model.Dao
{
    class EmployeeDao : AbstractDao
    {
        private static readonly CookieContainer CookieContainer = new CookieContainer();

        public static Employee FindById(string id)
        {
            Employee employee = null;
            CookieContainer.Add(Uri, AuthCookie);
            HttpResponseMessage result = Client.GetAsync("/example/" + id).Result;
            if (result.IsSuccessStatusCode)
            {
                string stringContent = result.Content.ReadAsStringAsync().Result;
                employee = JsonConvert.DeserializeObject<Employee>(stringContent);
                if (employee.ObjectId == null)
                {
                    employee = null;
                }
            }
            else
            {
                Console.WriteLine(result.ReasonPhrase);
            }
            return employee;
        }

        public static List<Employee> FindAll()
        {
            List<Employee> employees;
            CouchDBList<string, Employee> employeesDB;

            CookieContainer.Add(Uri, AuthCookie);
/*
 * View example for Employee
{
  "_id": "_design/_employeeView",
  "_rev": "4-362c7ecda4a89f0a5b7aecc0e8f9c73e",
  "views": {
    "employees": {
      "map": "function (doc) {\n  if (doc.type == \"employee\")\n  {\n    emit(doc._id, doc);\n  }\n}"
    }
  },
  "language": "javascript"
}
*/
            HttpResponseMessage result = Client.GetAsync("/example/_design/_employeeView/_view/employees").Result;

            if (result.IsSuccessStatusCode)
            {
                string stringContent = result.Content.ReadAsStringAsync().Result;

                employeesDB = JsonConvert.DeserializeObject<CouchDBList<string, Employee>>(stringContent);
                employees = employeesDB.Values;
            }
            else
            {
                employees = new List<Employee>();
                Console.WriteLine(result.ReasonPhrase);
            }
            return employees;
        }
    }
}
